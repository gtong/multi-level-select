(function($){
	$.fn.gselect = function(data){
		$(this[0]).gselect_render(data);
		for(var i=0;i<this.length-1;i++){
			$(this[i]).gselect_sub(this[i+1]);
		}
	}
	$.fn.gselect_render = function(data){
		$(this).empty();
		$(this).append("<option>请选择</option>");
		for(var i=0;i<data.children.length;i++){
			$(this).append("<option value='"+data.children[i].value+"'>"+data.children[i].name+"</option>")
		}
		$(this).data('gselect',data);
		$(this).trigger('change');
	}
	$.fn.gselect_sub = function(subdom){
		this.change(function(){
			var data =	$(this).data('gselect');
			for(var i=0;i<data.children.length;i++){
				if(data.children[i].value==$(this).val()){
					$(subdom).gselect_render(data.children[i]);					
				}
			}
		})
	}
})(jQuery)